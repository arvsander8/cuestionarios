<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ERROR !</title>
</head>
<body>
	<script> 
		switch (<% out.print(request.getParameter("error")); %>) {
		case 1:
			alert('Error: El usuario o password es incorrecto');
			document.location="login.jsp";		
			break;
		case 2:
			var pass1 = <% out.print(request.getParameter("pass1")); %>
			var pass2 = <% out.print(request.getParameter("pass2")); %>
			alert('Error: Las contraseņas no coinciden' + pass1 +" " + pass2);
			document.location="registrar.jsp";
		default:
			break;
		}
	
	</script>
</body>
</html>