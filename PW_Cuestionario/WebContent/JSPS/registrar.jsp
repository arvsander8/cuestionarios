<%@ include file="cabecera.jsp"%>

<script type="text/javascript">
	function checkForm() {
		
		// regular expression to match required date format
		re = /^[12][90][0-9][0-9]\/[01]?[0-9]\/[0-3]?[0-9]$/;

		if ($("#fecha").val().trim() == '') {
			alert("Formato de fecha No Valido (YYYY/MM/DD): ");
			$("#fecha").focus();
			return false;
		} else {
			if ($("#fecha").val().trim() != '' && !$("#fecha").val().match(re)) {
				alert("Formato de fecha No Valido (YYYY/MM/DD): ");
				$("#fecha").focus();
				return false;
			}
		}

		re2 = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		if ($("#email").val().trim() == '') {
			alert("Falta llenar el Email ");
			$("#email").focus();
			return false;
		} else {
			if ($("#email").val().trim() != '' && !$("#email").val().match(re2)) {
				alert("Ese correo no es Valido");
				$("#email").focus();
				return false;
			}
		}

		var nombre = $("#nombre").val().trim();
		var apellido1 = $("#apellido1").val().trim();
		var apellido2 = $("#apellido2").val().trim();
		var fecha = $("#fecha").val().trim();
		var email = $("#email").val().trim();
		var nick = $("#nick").val().trim();
		var clave1 = $("#pass1").val().trim();
		var clave2 = $("#pass2").val().trim();
		
		if (nombre != "" && apellido1 != "" && nick != "" && clave1 != ""
				&& clave2 != "") {
			if (clave1 != clave2) {
				alert("La contraseņa no coincide.");
				return false;
			}
		} else {
			alert("Debe Completar todos los campos");
			return false;
		}
		return true;

	}
	$(document).ready(function() {
		

		$("#enviar").click(function(data) {

			var nombre = $("#nombre").val().trim();
			var apellido1 = $("#apellido1").val().trim();
			var apellido2 = $("#apellido2").val().trim();
			var fecha = $("#fecha").val().trim();
			var email = $("#email").val().trim();
			var nick = $("#nick").val().trim();
			var clave1 = $("#pass1").val().trim();
			var clave2 = $("#pass2").val().trim();

			if (checkForm()) {
				$.get("../Registrar", {
					nombre:nombre,
					apellido1:apellido1,
					apellido2:apellido2,
					fecha:fecha,
					email:email,
					nick:nick,
					clave1:clave1
				}, function(data) {
					alert(data);
				});
			} else {
				alert("Ocurrio un error");
			}
		});
	});
</script>

<div id="registro" align="center">

	<form id="frm_registro" action="../Registrar" method="get">
		<table style="text-align: left; width: 300px; border-color: silver;"
			border="1" cellpadding="1" cellspacing="0">

			<tr>
				<th>Nombre</th>
				<td><input type="text" id="nombre" name="nombre" /></td>
			</tr>
			<tr>
				<th>Apellido Paterno</th>
				<td><input type="text" id="apellido1" name="apellido1" /></td>
			</tr>
			<tr>
				<th>Apellido Materno</th>
				<td><input type="text" id="apellido2" name="apellido2" /></td>
			</tr>
			<tr>
				<th>fecha Nacimiento</th>
				<td><input type="text" id="fecha" name="fecha" /></td>
			</tr>
			<tr>
				<th>E-mail</th>
				<td><input type="text" id="email" name="email" /></td>
			</tr>
			<tr>
				<th>Nick</th>
				<td><input type="text" id="nick" name="nick" /></td>
			</tr>
			<tr>
				<th>Clave</th>
				<td><input type="password" id="pass1" name="pass1" /></td>
			</tr>
			<tr>
				<th>Confirmar Clave</th>
				<td><input type="password" id="pass2" name="pass2" /></td>
			</tr>
			<tr>
				<td><input type="reset" value="Limpiar" /></td>
				<td><input type="button" id="enviar" name="enviar"
					value="Registrar" /></td>
			</tr>


		</table>
	</form>

</div>

<%@ include file="pie.jsp"%>