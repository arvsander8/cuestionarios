<%@ include file="cabecera.jsp"%>

<script>
	$(document).ready(function() {

		$("#div_cuestionario").hide();
		$("#div_nuevo").hide();

		$('#txt_curso').autocomplete({
			source : '../Llenar_Datos?tabla=course',
			minLength : 1,
			select : function(event, ui) {
				var id = ui.item.label.split("|")[0];
				;
				//var label = ui.item.label;

				$('#id_curso').val(id);
				//$('#txt_curso').val(label);

			}

		});
		$('#txt_cuestionario').autocomplete({
			source : '../Llenar_Datos?tabla=questionnaire',
			minLength : 1,
		});
		$("#btn_existente").click(function() {
			$("#div_cuestionario").show();
			$("#div_curso").hide();
		});

		$("#btn_nuevo").click(function() {
			$("#div_nuevo").show();
			$("#div_curso").hide();
		});
		
		$("#btn_nuevocurso").click(function(){
			var tema = $("#txt_tema");
			var curso = $("#txt_nombre");
			var attr = $("#cmb_atributo");
			$.get("../AgregarCurso",{tema:tema, curso:curso, attr:attr},function(data){
				alert(data);
			});
			
			$("#div_curso").show();
			
		});

	});
</script>

<style type="text/css">
.textarea {
	border-style: ridge;
	border-color: #000000;
	border-width: 3px";
}
</style>

<h3>Creaci�n de un nuevo Cuestionario o Curso</h3>

<div id="div_curso">
	<table id="tb_existente">
		<tr>
			<td>Curso Existente</td>
			<td><input type="hidden" id="id_curso" /><input type="text"
				id="txt_curso" /><input type="button" id="btn_existente"
				value="Agregar Cuestionario" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="button" id="btn_nuevo"
				value="Agregar un nuevo curso" /></td>
		</tr>
	</table>
	<br />
</div>

<div id="div_nuevo">
	<table id="tb_nuevo">
		<tr>
			<td>Tema-Area</td>
			<td><input type="text" id="txt_tema" /></td>
		</tr>
		<tr>
			<td>Nombre del Curso</td>
			<td><input type="text" id="txt_nombre" /></td>
		</tr>
		<tr>
			<td>Atributo</td>
			<td><select id="cmb_atributo">
					<option value="1">Publico</option>
					<option value="2">Privado</option>
			</select></td>
		</tr>
		<tr>
			<td>
				<input type="button" id="btn_nuevocurso" value="Agregar Nuevo Curso" />
			</td>
		</tr>
	</table>
	
</div>


<div id="div_cuestionario">

	<table>
		<tr>
			<td>Pregunta:</td>
			<td><textarea id="txt_pregunta" cols="50" rows="3"
					class="textarea"></textarea></td>
		</tr>

		<tr>
			<td>Alt. 1:</td>
			<td><textarea id="txt_alt1" cols="50" rows="1" class="textarea"></textarea>
			</td>
		</tr>
		<tr>
			<td>Alt. 2:</td>
			<td><textarea id="txt_alt2" cols="50" rows="1" class="textarea"></textarea>
			</td>
		</tr>
		<tr>
			<td>Alt. 3:</td>
			<td><textarea id="txt_alt3" cols="50" rows="1" class="textarea"></textarea>
			</td>
		</tr>
		<tr>
			<td>Alt. 4:</td>
			<td><textarea id="txt_alt4" cols="50" rows="1" class="textarea"></textarea>
			</td>
		</tr>
		<tr>
			<td>Alt. 5:</td>
			<td><textarea id="txt_alt5" cols="50" rows="1" class="textarea"></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center"><input type="button"
				id="btn_agregar" value="Agregar" /></td>
		</tr>
	</table>

</div>

<%@ include file="pie.jsp"%>