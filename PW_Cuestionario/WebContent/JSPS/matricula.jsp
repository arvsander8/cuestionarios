<%@ include file="cabecera.jsp"%>

<script>
	$(document).ready(function() {
		$('#txt_curso').autocomplete({
			source : function(request, response) {
				$.ajax({url : "../Llenar_Datos",dataType : "json",
					data : {term : request.term,tabla : "course"}});},
			select : function(event, ui) {
				var id = ui.item.label.split("|")[0];
				alert(term);
				$('#id_curso').val(id);
			}
		});
		$('#txt_cuestionario').autocomplete({
			source : '../Llenar_Datos?tabla=questionnaire',
			minLength : 1,
		});
		$("#btn_curso").click(function() {

		});

	});
</script>

<table id=tb_cursos border="1">
	<tr>
		<td>Buscar un Curso</td>
		<td><input id="id_curso" type="hidden" value="" /><input
			id="txt_curso" type="text" value="" /></td>
		<td><input type="button" id="btn_curso" value="Matricular" /></td>
	</tr>

	<tr>
		<td>Buscar un Cuestionario</td>
		<td><input id="id_cuestionario" type="hidden" value="" /><input
			id="txt_cuestionario" type="text" value="" /></td>
		<td><input type="button" id="btn_cuestionario" value="Resolver" /></td>
	</tr>

</table>

<%@ include file="pie.jsp"%>