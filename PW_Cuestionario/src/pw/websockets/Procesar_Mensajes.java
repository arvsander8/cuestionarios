package pw.websockets;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.PongMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;


@ServerEndpoint("/Procesar_Mensajes")
public class Procesar_Mensajes {

    private static final Set<Procesar_Mensajes> connections =
            new CopyOnWriteArraySet<Procesar_Mensajes>();
	private Session session;
    
	@OnOpen
	public void start(Session session) {
        this.session = session;
        connections.add(this);
        String message = String.format("User");
        //broadcast(message);
    }
	
	@OnMessage
	 public void incoming(String message) {
        // Never trust the client
        //String filteredMessage = String.format("%s: %s", message.toString());
        broadcast(message.toString());
    }
	
	@OnClose
    public void end() {
        connections.remove(this);
        String message = String.format("* %s %s","nickname", "has disconnected.");
        //broadcast(message);
    }

	
    private static void broadcast(String msg) {
        for (Procesar_Mensajes client : connections) {
            try {
                synchronized (client) {
                    client.session.getBasicRemote().sendText(msg);
                }
            } catch (IOException e) {
                //log.debug("Chat Error: Failed to send message to client", e);
                connections.remove(client);
                try {
                    client.session.close();
                } catch (IOException e1) {
                    // Ignore
                }
                //String message = String.format("* %s %s", " client.nickname", "has been disconnected.");
                //broadcast(message);
            }
        }
    }
    
}
