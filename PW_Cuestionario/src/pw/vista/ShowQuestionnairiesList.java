package pw.vista;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pw.funciones.Funciones;
import pw.modelo.JdbcConexion;

/**
 * Servlet implementation class ShowQuestionnairiesList
 */
@WebServlet("/ShowQuestionnairiesList")
public class ShowQuestionnairiesList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShowQuestionnairiesList() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		Connection myconexion = new JdbcConexion().init();
		PrintWriter out = response.getWriter();
		response.setContentType("text/plain");
		String _html = "";
		String user = request.getParameter("user");
		if (myconexion != null) {

			try {
				String sql = "Select * From questionnaire where id>0 order by id asc";
				// out.println(sql);
				Statement st = myconexion.createStatement();
				ResultSet rs = st.executeQuery(sql);
				Integer i = 0;
				_html = "<table border = 1>";
				while (rs.next()) {
					i = i + 1;
					String counter = new Funciones().getCoutQuestions(rs.getString("id")) ;
					_html = _html + "<tr><td>"+i+"</td><td><a id='cues'"+i+" href='#' onclick='mostrarCuestionario("+rs.getString("id")+")'>"+rs.getString("title") + "</a></td><td>( "+counter+"preguntas)</td></tr>";
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}

		} else {
			out.println("Conexion insatisfactoria a la base de datos");
		}

		// response.sendRedirect(_html);;
		out.print(_html);
		out.close();

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
