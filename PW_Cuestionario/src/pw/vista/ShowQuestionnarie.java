package pw.vista;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pw.modelo.JdbcConexion;

/**
 * Servlet implementation class ShowQuestionnarie
 */
@WebServlet("/ShowQuestionnarie")
public class ShowQuestionnarie extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShowQuestionnarie() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Connection myconexion = new JdbcConexion().init();
		PrintWriter out = response.getWriter();
		response.setContentType("text/plain");
		String id = request.getParameter("id");
		String _html = "";
		if (myconexion != null) {

			try {
				String sql = "Select * From question where \"idQuestionnaire\"="+id+" order by id asc";
				// out.println(sql);
				Statement st = myconexion.createStatement();
				Statement st2 = myconexion.createStatement();
				ResultSet rs = st.executeQuery(sql);
				Integer i = 0;
				while (rs.next()) {
					i= i + 1;
					_html = _html + rs.getString("question") + "<br>";
					
					String sql2 = "Select * From alternative where \"idQuestion\"="
							+ rs.getString("id");
					ResultSet rs2 = st2.executeQuery(sql2);
					Integer letra = 0;
					_html = _html + "<select name='p"+ i +"' id='p"+ i +"'>";// Falta
																	// trabajar
																	// con
																	// matriz de
																	// controles
					_html = _html
							+ "<option value=0>Escoja su Respuesta</option>";
					while (rs2.next()) {
						letra = letra + 1;
						_html = _html + letra + ")" + "<option value=" + letra/*rs2.getString("id")*/
								+ ">" + rs2.getString("description")
								+ "</option>";
					}
					_html = _html + "</select><br>";
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}

		} else {
			out.println("Conexion insatisfactoria a la base de datos");
		}
		
		//response.sendRedirect(_html);;
		out.print(_html);
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
