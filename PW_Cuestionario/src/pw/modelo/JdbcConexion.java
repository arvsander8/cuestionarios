package pw.modelo;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;


public class JdbcConexion {
	private  Connection conexion= null;
	public static Connection stConexion=null;
	public JdbcConexion(){
		
	}
	public Connection init(){
		try {		 
			Class.forName("org.postgresql.Driver");
			System.out.println("Driver Postgresql registrado correctamente -----!");
			try {
				conexion = DriverManager.getConnection(
						"jdbc:postgresql://127.0.0.1:5432/cuestionarios", "postgres",
						"sander7l");
				if (conexion != null) {
					System.out.println("Conexion satisfactoria, comenzando interaccion con bd.");
				} else {
					System.out.println("Fallo en hacer la conexion :( !");
				}
				return conexion;
	 
			} catch (SQLException e) {
				System.out.println("Conexion fallida! Revisar la consola para informacion");
				e.printStackTrace();
			}

		} catch (ClassNotFoundException e) {
			System.out.println("El driver pgsql no se encuentra en el path!! "+ "Incluirlo por favor.");
			e.printStackTrace();
		}
		stConexion=conexion;
		return conexion;
	}	    		
}

