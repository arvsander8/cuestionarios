package pw.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;





//Jdbc Driver
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;





import pw.modelo.JdbcConexion;


//Servlet communication
import java.io.PrintWriter;

/**
 * Servlet implementation class login
 */
@WebServlet("/Registrar")
public class Registrar extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Registrar() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Connection myconexion = new JdbcConexion().init();
		PrintWriter out = response.getWriter();
		//HttpSession sesion = request.getSession();

		if (myconexion != null) {

			String nick = request.getParameter("nick");
			String nombre = request.getParameter("nombre");
			String apellido1 = request.getParameter("apellido1");
			String apellido2 = request.getParameter("apellido2");
			String fecha_nac = request.getParameter("fecha");
			//String email = request.getParameter("email");
			String pass1 = request.getParameter("clave1");
	
		

			String seq = "select max(id) as id from \"user\"";

			// response.sendRedirect("JSPS/cuestionario.jsp");

			try {
				Statement st = myconexion.createStatement();
				ResultSet rs = st.executeQuery(seq);
				if (rs.next()) {
					String id = rs.getString("id");
					/*
					 * String insertar = "INSERT INTO \"user\"  VALUES (" + id +
					 * ", " + id + ", '" + nombre + "', '" + apellido1 + "', '"
					 * + apellido2 + "', '" + fecha_nac +
					 * "', "+pass1+",1, '"+nick+"');"; ResultSet rs2 =
					 * st.executeUpdate(insertar);
					 */
					PreparedStatement pst = myconexion.prepareStatement("INSERT into \"user\"  VALUES (?,?,?,?,?,?,?,?,?)");

					pst.setInt(1, Integer.parseInt(id)+1);
					pst.setInt(2, Integer.parseInt(id)+1);
					pst.setString(3, nombre);
					pst.setString(4, apellido1);
					pst.setString(5, apellido2);
					
					//Para setear las fechas
					SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
					java.util.Date  fec = formato.parse(fecha_nac);
					Date sqlfec = new Date(fec.getTime());
					pst.setDate(6, sqlfec);
					
					pst.setString(7, pass1);
					pst.setInt(8, 1);
					pst.setString(9, nick);

					int numRowsChanged = pst.executeUpdate();
					if (numRowsChanged != 0) {
						out.print("Record has been inserted");
					} else {
						out.print("failed to insert the data");
					}
					pst.close();

				} else {
					out.print("Ocurrio un error en el servidor");
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}

		} else {
			out.println("Conexion insatisfactoria a la base de datos");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
