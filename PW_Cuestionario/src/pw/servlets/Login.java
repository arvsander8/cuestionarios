package pw.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



//Jdbc Driver
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;




import pw.modelo.JdbcConexion;


//Servlet communication
import java.io.PrintWriter;

/**
 * Servlet implementation class login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Connection myconexion = new JdbcConexion().init();
		PrintWriter out = response.getWriter();
		HttpSession sesion = request.getSession();

		if (myconexion != null) {
			out.println("Conexion satisfactoria a la base de datos asi:");
			String user = request.getParameter("usuario");
			String pass = request.getParameter("clave");
			try {
				String sql = "Select * From \"user\" where nick='" + user
						+ "' and password='" + pass + "'";
				//out.println(sql);
				Statement st = myconexion.createStatement();
				ResultSet rs = st.executeQuery(sql);
				if (rs.next()) {
					sesion.setAttribute("usuario", user);
					response.sendRedirect("JSPS/cuestionario.jsp");
				} else {
					response.sendRedirect("JSPS/error.jsp?error=1");
				}
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}

		} else {
			out.println("Conexion insatisfactoria a la base de datos");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
