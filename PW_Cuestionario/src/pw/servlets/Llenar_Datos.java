package pw.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pw.funciones.Funciones;

/**
 * Servlet implementation class Llenar_Datos
 */
@WebServlet("/Llenar_Datos")
public class Llenar_Datos extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Llenar_Datos() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
		// Get the printwriter object from response to write the required json
		// object to the output stream
		PrintWriter out = response.getWriter();
		String jsonObject = "";
		String tabla = request.getParameter("tabla");
		String term = request.getParameter("term");
		
		switch (tabla) {
		case "questionnaire":
			jsonObject = new Funciones().getQuestionnaireList(term);
			break;
		case "course":
			jsonObject = new Funciones().getCoursesList(term);
			break;
		default:
			jsonObject = "JSON Objeto no definido";
			break;
		}
		// Assuming your json object is **jsonObject**, perform the following,
		// it will return your json object
		out.print(jsonObject);
		out.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
