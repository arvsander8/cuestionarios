package pw.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pw.modelo.JdbcConexion;

import com.sun.org.apache.xalan.internal.xsltc.compiler.Parser;

/**
 * Servlet implementation class Calificacion
 */
@WebServlet("/Calificacion")
public class Calificacion extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Calificacion() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		Connection myconexion = new JdbcConexion().init();
		PrintWriter out = response.getWriter();
		response.setContentType("text/plain");
		// HttpSession sesion = request.getSession();

		if (myconexion != null) {

			String cuest = "1";// request.getParameter("cuest");

			/*
			 * String p1 = request.getParameter("p1"); String p2 =
			 * request.getParameter("p2"); String p3 =
			 * request.getParameter("p3"); String p4 =
			 * request.getParameter("p4"); String p5 =
			 * request.getParameter("p5"); String p6 =
			 * request.getParameter("p6"); String p7 =
			 * request.getParameter("p7"); String p8 =
			 * request.getParameter("p8"); String p9 =
			 * request.getParameter("p9"); String p10 =
			 * request.getParameter("p10");
			 */

			try {
				String sql = "select id,\"alternativeCorrect\" from question where \"idQuestionnaire\"=1 order by id";
				Statement st = myconexion.createStatement();
				ResultSet rs = st.executeQuery(sql);
				Integer i = 0;
				Integer nota = 0;
				Integer user_rpt = 0;
				while (rs.next()) {
					i = i + 1;
					System.out.println(i);
					String id = rs.getString("id");
					String rpt = rs.getString("alternativeCorrect");
					user_rpt = Integer.parseInt(request.getParameter("p"+i));
					if (Integer.parseInt(rpt) == user_rpt) {
						nota = nota + 2;
					}

				}
				out.print("La nota es:" + nota);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}

		} else {
			out.println("Conexion insatisfactoria a la base de datos");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
